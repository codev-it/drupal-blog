<?php
/** @noinspection PhpHierarchyChecksInspection */

namespace Drupal\Tests\codev_blog\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FunctionalTestBase.php
 * .
 */

/**
 * Class FunctionalTestBase.
 *
 * Default functional test base settings.
 *
 * @package      Drupal\Tests\codev_blog\Functional
 *
 * @group        codev_blog
 *
 * @noinspection PhpUnused
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_blog',
  ];

  /**
   * An array of config object names that are excluded from schema checking.
   *
   * @var string[]
   */
  protected static $configSchemaCheckerExclusions = [
    'field.storage.paragraph.field_view',
    'field.field.paragraph.view.field_view',
    'core.entity_view_display.paragraph.view.default',
    'core.entity_form_display.block_content.basic.default',
  ];

}

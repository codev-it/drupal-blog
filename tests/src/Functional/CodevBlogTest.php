<?php

namespace Drupal\Tests\codev_blog\Functional;

use Drupal\taxonomy\Entity\Term;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevBlogTest.php
 * .
 */

/**
 * Class CodevBlogTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_blog\Functional
 *
 * @group        codev_blog
 *
 * @noinspection PhpUnused
 */
class CodevBlogTest extends FunctionalTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser();
  }

  /**
   * Tests the functionality.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testCodevBlog() {
    // Defaults
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('node/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists(t('Blog'));

    // Create tag term
    $term = Term::create([
      'vid'  => 'blog_tags',
      'name' => t('Blog taxonomy'),
    ]);

    // Node blog
    $this->drupalGet('node/add/blog');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Title'));
    $this->assertSession()->pageTextContains(t('Tags'));
    $this->assertSession()->pageTextContains(t('Body'));
    $node = $this->drupalCreateNode([
      'type'            => 'blog',
      'title'           => t('Blog node'),
      'body_summary'    => t('Blog summary'),
      'field_blog_tags' => $term->id(),
    ]);
    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    // Create overview block
    $this->drupalGet('admin/structure/block/add/views_block:blog-block_archive');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['id' => 'block_archive'], t('Save block'));
    $this->drupalPlaceBlock('views_block:blog-block_archive');

    // Create archive block
    $this->drupalGet('admin/structure/block/add/views_block:blog-block_overview');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['id' => 'block_overview'], t('Save block'));
    $this->drupalPlaceBlock('views_block:blog-block_overview');

    // Create tags block
    $this->drupalGet('admin/structure/block/add/views_block:blog_tags-block_tags');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm(['id' => 'blog_tags'], t('Save block'));
    $this->drupalPlaceBlock('views_block:blog_tags-block_tags');

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet('');

    // Check if blocks exists
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Blog'));
    $this->assertSession()->pageTextContains(t('Blog archive'));
    $this->assertSession()->pageTextContains(t('Blog tags'));

    // Check if node exists
    $this->assertSession()->linkExists(t('Blog node'));
    $this->assertSession()->pageTextContains(t('Blog summary'));
  }

}

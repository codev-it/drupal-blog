<?php

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_blog.views.inc
 * .
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_pre_execute().
 *
 * @noinspection PhpUnused
 */
function codev_blog_views_pre_render(ViewExecutable $view) {
  if ($view->id() == 'blog_tags') {
    $db = Drupal::database();
    $query = $db->select('node__field_blog_tags', 't');
    $query->fields('t', ['field_blog_tags_target_id']);
    $ids = $query->distinct()->execute()->fetchCol();
    foreach ($view->result as $row => $result) {
      if (!in_array($result->_entity->id(), $ids)) {
        unset($view->result[$row]);
      }
    }
  }
}
